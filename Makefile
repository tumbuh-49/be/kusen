checkENV:
	echo "postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}"

migrate\:up:
	migrate -path migrations -database "postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}?sslmode=disable" -verbose up

migrate\:down:
	migrate -path migrations -database "postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}?sslmode=disable" -verbose down

docker\:up:
	docker compose -f ../docker-compose.yaml -p tumbuh up -d

docker\:down:
	docker compose -p tumbuh down

dev\:run:
	go run cmd/server/main.go

psql:
	export PGPASSWORD=${POSTGRES_PASSWORD} && psql -h ${POSTGRES_HOST} -U ${POSTGRES_USER} -w -d ${POSTGRES_DB}

.PHONY: checkENV migrate\:up migrate\:down docker\:up docker\:down dev\:run psql
