CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE SCHEMA IF NOT EXISTS public;

DO $$ 
BEGIN 
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'transaction_type') THEN 
        CREATE TYPE "transaction_type" AS ENUM ('in', 'out');
    END IF; 
END $$;


CREATE TABLE IF NOT EXISTS "users" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
  "full_name" varchar NOT NULL,
  "email" varchar UNIQUE NOT NULL,
  "password" varchar NOT NULL,
  "created_at" timestamptz NOT NULL,
  "isVerify" boolean NOT NULL DEFAULT false
);

CREATE TABLE IF NOT EXISTS "reports" (
  "id" uuid PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4 (),
  "sheet_id" varchar,
  "created_at" timestamptz NOT NULL DEFAULT 'now()'
);

CREATE TABLE IF NOT EXISTS "users_reports" (
  "user_id" uuid NOT NULL,
  "report_id" uuid NOT NULL,
  PRIMARY KEY ("user_id", "report_id")
);

CREATE TABLE IF NOT EXISTS "transactions" (
  "id" uuid PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4 (),
  "report_id" uuid NOT NULL,
  "bucket_id" uuid NOT NULL,
  "wallet_id" uuid NOT NULL,
  "category_id" uuid NOT NULL,
  "budget_id" uuid NOT NULL,
  "related_id" uuid,
  "date" timestamptz NOT NULL DEFAULT 'now()',
  "amount" bigint NOT NULL DEFAULT 0,
  "fee" int DEFAULT 0,
  "notes" varchar,
  "created_at" timestamptz NOT NULL DEFAULT 'now()'
);

CREATE TABLE IF NOT EXISTS "buckets" (
  "id" uuid PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4 (),
  "label" varchar NOT NULL,
  "goals_target" bigint,
  "created_at" timestamptz NOT NULL DEFAULT 'now()'
);

CREATE TABLE IF NOT EXISTS "wallets" (
  "id" uuid PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4 (),
  "label" varchar NOT NULL,
  "icon" varchar,
  "created_at" timestamptz NOT NULL DEFAULT 'now()'
);

CREATE TABLE IF NOT EXISTS "categories" (
  "id" uuid PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4 (),
  "budget_id" uuid NOT NULL,
  "label" varchar NOT NULL,
  "type" transaction_type NOT NULL,
  "created_at" timestamptz NOT NULL DEFAULT 'now()'
);

CREATE TABLE IF NOT EXISTS "budgets" (
  "id" uuid PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4 (),
  "label" varchar NOT NULL,
  "amount" bigint NOT NULL,
  "created_at" timestamptz NOT NULL DEFAULT 'now()'
);

COMMENT ON COLUMN "reports"."sheet_id" IS 'For excel/gsheet file id';

DO $$ 
BEGIN 
    IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'users_reports_user_id_fkey') THEN 
        ALTER TABLE "users_reports" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");
    END IF; 
    
    IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'users_reports_report_id_fkey') THEN 
        ALTER TABLE "users_reports" ADD FOREIGN KEY ("report_id") REFERENCES "reports" ("id");
    END IF; 
    
    IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'transactions_report_id_fkey') THEN 
        ALTER TABLE "transactions" ADD FOREIGN KEY ("report_id") REFERENCES "reports" ("id");
    END IF; 
    
    IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'transactions_bucket_id_fkey') THEN 
        ALTER TABLE "transactions" ADD FOREIGN KEY ("bucket_id") REFERENCES "buckets" ("id");
    END IF; 
    
    IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'transactions_wallet_id_fkey') THEN 
        ALTER TABLE "transactions" ADD FOREIGN KEY ("wallet_id") REFERENCES "wallets" ("id");
    END IF; 
    
    IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'transactions_category_id_fkey') THEN 
        ALTER TABLE "transactions" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("id");
    END IF; 
    
    IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'transactions_budget_id_fkey') THEN 
        ALTER TABLE "transactions" ADD FOREIGN KEY ("budget_id") REFERENCES "budgets" ("id");
    END IF; 
    
    IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'transactions_related_id_fkey') THEN 
        ALTER TABLE "transactions" ADD FOREIGN KEY ("related_id") REFERENCES "transactions" ("id");
    END IF; 
    
    IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'categories_budget_id_fkey') THEN 
        ALTER TABLE "categories" ADD FOREIGN KEY ("budget_id") REFERENCES "budgets" ("id");
    END IF; 
END $$;