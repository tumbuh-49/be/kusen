package main

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func main() {
	e := echo.New()

	api := e.Group("/api")
	api.GET("/health", func(c echo.Context) error {
		return c.String(http.StatusOK, "Server is running")
	})

	e.Logger.Fatal(e.Start(":4000"))
}
